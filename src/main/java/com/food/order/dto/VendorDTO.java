package com.food.order.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class VendorDTO {

	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Itemname must contains only Alphanumeric")
	@NotNull(message = "Enter VendorName to add the Vendor")
	private String vendorName;

	@Pattern(regexp = "^[a-zA-Z]+$", message = "Origin must contains only Alphabets")
	@NotNull(message = "Enter Country of Origin to add the Vendor")
	private String origin;

	@Pattern(regexp = "^[\\d]+$", message = "Since is an Year so enter 0 to 9 numbers")
	@NotNull(message = "Enter Started Year(Since) to add the Vendor")
	private String since;

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getSince() {
		return since;
	}

	public void setSince(String since) {
		this.since = since;
	}

}
