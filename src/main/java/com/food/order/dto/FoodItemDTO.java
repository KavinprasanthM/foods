package com.food.order.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodItemDTO {
	
	@Pattern(regexp = "^[a-zA-Z0-9]+$",message="Itemname must contains only Alphanumeric")
	@NotBlank(message = "Enter ItemName to add the FoodItem in Menu")
	private String itemName;
	
	@NotBlank(message = "Enter ItemType to add FoodItem in Menu")
	@Pattern(regexp = "^[a-zA-Z]+$",message="ItemType must contains only Alphabets")
	private String itemType;
	
	private int itemPrice;

	public FoodItemDTO(String string, double d, String string2) {
		// TODO Auto-generated constructor stub
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	

}
