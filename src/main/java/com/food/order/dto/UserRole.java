package com.food.order.dto;

public enum UserRole {
	EMPLOYEE,
	VENDOR_ADMIN,
	ADMIN

}
