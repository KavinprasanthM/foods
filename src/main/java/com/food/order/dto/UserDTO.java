package com.food.order.dto;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserDTO {

	@NotBlank(message = "Enter UserName to Register the User")
	@Pattern(regexp = "^[a-zA-Z]+$", message = "UserNmae must contains only Alphabets")
	private String userName;

	@NotBlank(message = "Enter MailId to Register the User")
	@Pattern(regexp = "^[a-z]+@[a-z]+\\.[a-z]+$", message = "Emailid must contains lowercase,@,(.)")

	private String emailId;

	@Pattern(regexp = "^[\\d]+$", message = "PhoneNumber must contain numbers")
	@NotBlank(message = "Enter Phone number to Register the User")
	private String phoneNo;

	@Enumerated(EnumType.STRING)
	private UserRole userRole;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	
}
