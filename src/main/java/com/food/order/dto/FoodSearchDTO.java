package com.food.order.dto;

import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodSearchDTO {

	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Itemname must contains only Alphanumeric")
	private String itemName;

	@Pattern(regexp = "^[a-zA-Z]+$", message = "ItemType must contains only Alphabets")
	private String itemType;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	
}
