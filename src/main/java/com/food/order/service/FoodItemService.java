package com.food.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.order.dto.FoodSearchDTO;
import com.food.order.entity.FoodItem;
import com.food.order.exception.FoodNotFoundException;
import com.food.order.repository.FoodRepository;

import io.micrometer.common.util.StringUtils;

@Service
public class FoodItemService {

	@Autowired
	private FoodRepository foodRepository;

	public List<FoodItem> searchFoods(FoodSearchDTO searchDTO) {
		List<FoodItem> result;

		if (StringUtils.isNotBlank(searchDTO.getItemName())) {
			result = foodRepository.findByitemName(searchDTO.getItemName());
		} else if (StringUtils.isNotBlank(searchDTO.getItemType())) {
			result = foodRepository.findByitemType(searchDTO.getItemType());

		} else {
			throw new IllegalArgumentException("At least one search parameter is required");
		}

		if (result.isEmpty()) {
			throw new FoodNotFoundException("Sorry No food is found");
		}

		return result;
	}

}
