package com.food.order.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.order.dto.FoodItemDTO;
import com.food.order.dto.LoginStatus;
import com.food.order.dto.ResponseDTO;
import com.food.order.dto.UserRole;
import com.food.order.entity.FoodItem;
import com.food.order.entity.UserRegistration;
import com.food.order.entity.Vendor;
import com.food.order.exception.UserAlreadyExists;
import com.food.order.exception.UserNotFoundException;
import com.food.order.repository.FoodRepository;
import com.food.order.repository.UserRepository;
import com.food.order.repository.VendorRepository;

@Service
public class FoodAddService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FoodRepository foodRepository;
	@Autowired
	private VendorRepository vendorRepository;

	public ResponseDTO addFood(String userName, int vendorId, FoodItemDTO foodDto) {

		Optional<UserRegistration> registrationOptional = userRepository.findByUserNameAndLogAndUserRole(userName,
				LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN);

		Optional<Vendor> vendor = vendorRepository.findByVendorId(vendorId);
		
		if (registrationOptional.isPresent() && vendor.isPresent()) {

			Optional<FoodItem> food = foodRepository.findByItemName(foodDto.getItemName());
			if (food.isPresent()) {
				throw new UserAlreadyExists("Food Already Added");
			}

			FoodItem foods = new FoodItem();
			foods.setItemName(foodDto.getItemName());
			foods.setItemPrice(foodDto.getItemPrice());
			foods.setItemType(foodDto.getItemType());
			
			foods.setVendor(vendor.get());

			foodRepository.save(foods);

			return new ResponseDTO();

		} else
			throw new UserNotFoundException(
					"Please check user is Loggedin and also have a Vendor_Admin credential (or) check User is Already Registered(or)Vendor is Present");
	}
}
