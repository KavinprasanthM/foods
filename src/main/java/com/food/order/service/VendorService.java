package com.food.order.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.order.dto.LoginStatus;
import com.food.order.dto.ResponseDTO;
import com.food.order.dto.UserRole;
import com.food.order.dto.VendorDTO;
import com.food.order.entity.UserRegistration;
import com.food.order.entity.Vendor;
import com.food.order.exception.UserAlreadyExists;
import com.food.order.exception.UserNotFoundException;
import com.food.order.repository.UserRepository;
import com.food.order.repository.VendorRepository;

@Service
public class VendorService {
	@Autowired
	private VendorRepository vendorRepository;
	@Autowired
	private UserRepository userRepository;

	public ResponseDTO addvendor(String userName, VendorDTO vendorDto) {
		Optional<UserRegistration> registrationOptional = userRepository
				.findByUserNameAndLogAndUserRole(userName, LoginStatus.LoggedIn, UserRole.ADMIN);
		if (registrationOptional.isPresent()) {
		

			if (vendorRepository.existsByvendorName(vendorDto.getVendorName())) {
				throw new UserAlreadyExists("vendor is already exists");
			}
			Vendor vendor = new Vendor();
			vendor.setVendorName(vendorDto.getVendorName());
			vendor.setOrigin(vendorDto.getOrigin());
			vendor.setSince(vendorDto.getSince());

			vendorRepository.save(vendor);

			return new ResponseDTO();
		} else {
			throw new UserNotFoundException(
					"Please check user is Loggedin and also have a Admin credential Us(or) check User is Already Registered");
		}
	}

}