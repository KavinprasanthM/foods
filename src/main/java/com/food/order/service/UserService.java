package com.food.order.service;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.order.dto.LoginDTO;
import com.food.order.dto.LoginStatus;
import com.food.order.dto.ResponseDTO;
import com.food.order.dto.UserDTO;
import com.food.order.entity.UserRegistration;
import com.food.order.exception.UserAlreadyExists;
import com.food.order.exception.UserNotFoundException;
import com.food.order.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public ResponseDTO register(UserDTO userDto) {
		Optional<UserRegistration> user = userRepository.findByuserName(userDto.getUserName());
		if (user.isPresent()) {
			throw new UserAlreadyExists("User already Registered");
		}
		UserRegistration users = new UserRegistration();
		users.setUserName(userDto.getUserName());
		users.setEmailId(userDto.getEmailId());
		users.setPhoneNo(userDto.getPhoneNo());
		users.setLog(LoginStatus.LoggedOut);
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(8, characters);
		users.setPassword(pwd);
		users.setUserRole(userDto.getUserRole());

		userRepository.save(users);
		return new ResponseDTO();
	}

	public ResponseDTO login(LoginDTO loginDto) {
		org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
		System.out.println(loginDto.toString());
		// logger.info(loginDto.toString());
		UserRegistration userRegistration = userRepository.findByuserNameAndPassword(loginDto.getUserName(),
				loginDto.getPassword());
		System.err.println(userRegistration);
		if (userRegistration != null && loginDto.getPassword().equals(userRegistration.getPassword())) {
			userRegistration.setLog(LoginStatus.LoggedIn);
			userRepository.save(userRegistration);
			logger.info("User Logged in successfully");
			return new ResponseDTO();
		} else {
			logger.error("Invalid Credentials or User Not found");
			throw new UserNotFoundException("Invalid Credentials or User Not Found.");  
		}
	}

}
