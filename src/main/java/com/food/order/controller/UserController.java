package com.food.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.order.dto.LoginDTO;
import com.food.order.dto.ResponseDTO;
import com.food.order.dto.UserDTO;
import com.food.order.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api")
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping("/user")
	public ResponseEntity<ResponseDTO> register(@RequestBody @Valid UserDTO userDTO) {
		return new ResponseEntity<>(userService.register(userDTO), HttpStatus.CREATED);
	}

	@PostMapping("/login")
	public ResponseEntity<ResponseDTO> userLogin(@RequestBody @Valid LoginDTO loginDto) {
		return new ResponseEntity<>(userService.login(loginDto), HttpStatus.OK);
	}
}
