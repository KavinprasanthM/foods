package com.food.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.order.dto.ResponseDTO;
import com.food.order.dto.VendorDTO;
import com.food.order.service.VendorService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api")
public class VendorController {

	@Autowired
	private VendorService vendorService;

	@PostMapping("/vendor")
	public ResponseEntity<ResponseDTO> addingvendor(@RequestParam String userName,
			@RequestBody @Valid VendorDTO vendorDto) {

		ResponseDTO responsedto = vendorService.addvendor(userName, vendorDto);
		return new ResponseEntity<>(responsedto, HttpStatus.CREATED);
	}
}
