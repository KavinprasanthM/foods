package com.food.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.order.dto.FoodSearchDTO;
import com.food.order.entity.FoodItem;
import com.food.order.service.FoodItemService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class FoodItemController {
	
	@Autowired
	private FoodItemService foodItemService;

	@GetMapping("/foods")
	public ResponseEntity<List<FoodItem>> searchFoods(@ModelAttribute @Valid FoodSearchDTO searchDTO) {
		List<FoodItem> searchResult = foodItemService.searchFoods(searchDTO);
		return new ResponseEntity<>(searchResult, HttpStatus.OK);
	}
	

}
