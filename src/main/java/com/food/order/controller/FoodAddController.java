package com.food.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.order.dto.FoodItemDTO;
import com.food.order.dto.ResponseDTO;
import com.food.order.service.FoodAddService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api")

public class FoodAddController {

	@Autowired
	private FoodAddService foodAddService;

	@PostMapping("/food")
	public ResponseEntity<ResponseDTO> addingfood(@RequestParam String userName, @RequestParam int vendorId,
			@RequestBody @Valid FoodItemDTO foodDto) {

		ResponseDTO responsedto = foodAddService.addFood(userName,vendorId, foodDto);
		return new ResponseEntity<>(responsedto, HttpStatus.CREATED); 
	}
}