package com.food.order.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.order.entity.FoodItem;

public interface FoodRepository extends JpaRepository<FoodItem, Integer> {

	List<FoodItem> findByitemType(String itemType);

	List<FoodItem> findByitemName(String itemName);

	Optional<FoodItem> findByItemName(String itemName);

	Optional<FoodItem> findByFoodId(int foodId);

}
