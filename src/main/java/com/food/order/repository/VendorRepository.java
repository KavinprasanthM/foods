package com.food.order.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.order.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Integer> {

	boolean existsByvendorName(String vendorName);

	Optional<Vendor> findByVendorId(int vendorId);

}
