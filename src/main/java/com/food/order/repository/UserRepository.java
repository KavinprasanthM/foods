package com.food.order.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.order.dto.LoginStatus;
import com.food.order.dto.UserRole;
import com.food.order.entity.UserRegistration;

public interface UserRepository extends JpaRepository<UserRegistration, Integer> {

	Optional<UserRegistration> findByuserName(String userName);

	UserRegistration findByuserNameAndPassword(String userName, String password);

	Optional<UserRegistration> findByUserNameAndLogAndUserRole(String userName, LoginStatus loggedin, UserRole admin);

	//Optional<FoodItem> findByItemName(String itemName);

}
