package com.food.order.Foodaddtest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.food.order.dto.FoodItemDTO;
import com.food.order.dto.LoginStatus;
import com.food.order.dto.ResponseDTO;
import com.food.order.dto.UserRole;
import com.food.order.entity.FoodItem;
import com.food.order.entity.UserRegistration;
import com.food.order.entity.Vendor;
import com.food.order.exception.UserAlreadyExists;
import com.food.order.exception.UserNotFoundException;
import com.food.order.repository.FoodRepository;
import com.food.order.repository.UserRepository;
import com.food.order.repository.VendorRepository;
import com.food.order.service.FoodAddService;

@ExtendWith(MockitoExtension.class)
class FoodAddServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private FoodRepository foodRepository;

    @Mock
    private VendorRepository vendorRepository;

    @InjectMocks
    private FoodAddService foodAddService;

    @Test
    void testAddFood_Success() {
        // Arrange
        String userName = "admin";
        int vendorId = 123;
        FoodItemDTO foodDto = new FoodItemDTO("Pizza", 10.99, "Italian");
        UserRegistration user = new UserRegistration(userName, LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN);
        Vendor vendor = new Vendor(vendorId, "Vendor Name");
        FoodItem food = new FoodItem("Pizza", 10.99, "Italian");
        
        when(userRepository.findByUserNameAndLogAndUserRole(userName, LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN))
            .thenReturn(Optional.of(user));
        when(vendorRepository.findByVendorId(vendorId)).thenReturn(Optional.of(vendor));
        when(foodRepository.findByItemName("Pizza")).thenReturn(Optional.empty());
        when(foodRepository.save(any())).thenReturn(food);

        // Act
        ResponseDTO response = foodAddService.addFood(userName, vendorId, foodDto);

        // Assert
        assertEquals(201, response.getStatusCode());
        assertEquals("Food Added sucessfully", response.getMessage());
        verify(foodRepository, times(1)).save(any());
    }

    @Test
    void testAddFood_FoodAlreadyExists() {
        // Arrange
        String userName = "admin";
        int vendorId = 123;
        FoodItemDTO foodDto = new FoodItemDTO("Pizza", 10.99, "Italian");
        UserRegistration user = new UserRegistration(userName, LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN);
        Vendor vendor = new Vendor(vendorId, "Vendor Name");
        
        when(userRepository.findByUserNameAndLogAndUserRole(userName, LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN))
            .thenReturn(Optional.of(user));
        when(vendorRepository.findByVendorId(vendorId)).thenReturn(Optional.of(vendor));
        when(foodRepository.findByItemName("Pizza")).thenReturn(Optional.of(new FoodItem()));

        // Act & Assert
        assertThrows(UserAlreadyExists.class, () -> {
            foodAddService.addFood(userName, vendorId, foodDto);
        });
        verify(foodRepository, never()).save(any());
    }

    @Test
    void testAddFood_UserNotFound() {
        // Arrange
        String userName = "admin";
        int vendorId = 123;
        FoodItemDTO foodDto = new FoodItemDTO("Pizza", 10.99, "Italian");

        when(userRepository.findByUserNameAndLogAndUserRole(userName, LoginStatus.LoggedIn, UserRole.VENDOR_ADMIN))
            .thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(UserNotFoundException.class, () -> {
            foodAddService.addFood(userName, vendorId, foodDto);
        });
        verify(foodRepository, never()).save(any());
    }
}